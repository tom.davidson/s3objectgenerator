import * as crypto from "crypto";

export function rand(v: number): string {
  function chunk(i: number) {
    return crypto.randomBytes(Math.ceil(Math.random() * 10 * i * 3 / 4))
      .toString("base64")
      .replace(/\+/g, "/")
      .replace(/\=/g, "/")
      .replace(/\/\//g, "/");
  }
  return (v % 2 === 0) ? chunk(v * 3) + "/" + chunk(v * 2) : chunk(v * 1);
}
