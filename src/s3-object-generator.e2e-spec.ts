import * as S3 from "aws-sdk/clients/s3";
import * as chai from "chai";
import * as chaiAsPromised from "chai-as-promised";
import {
  generate,
  generateSync,
  IS3ObjectGenerateRequest,
} from "./index";

chai.use(chaiAsPromised);
const expect = chai.expect;

// mocha needs 'this' to set timeout
// tslint:disable-next-line:only-arrow-functions
describe("S3ObjectGenerate e2e", function () { // mocha needs 'this; to set timeout
  this.timeout(5000);
  const s3 = new S3({
    apiVersion: "2006-03-01",
  });

  const testBucketName = "S3ObjectGenerator-TestBucket";

  before(() => {
    // tslint:disable-next-line:no-console
    console.log("    Setup - create S3 Bucket");
    return s3.createBucket({ Bucket: testBucketName }).promise()
      .then((data) => { return Promise.resolve(); })
      .catch((err) => { return Promise.reject(err); });
  });

  after(() => {
    // tslint:disable-next-line:no-console
    console.log("    Teardown - destroy S3 Bucket");
    return s3.deleteBucket({ Bucket: testBucketName }).promise()
      .then((data) => { return Promise.resolve(); })
      .catch((err) => { return Promise.reject(err); });
  });

  // afterEach(() => {
  //   // tslint:disable-next-line:no-console
  //   console.log("    Cleanup - emptying test S3 Bucket");
  //   return s3.deleteBucket({ Bucket: testBucketName }).promise()
  //     .then((data) => { return Promise.resolve(); })
  //     .catch((err) => { return Promise.reject(err); });
  // });

    // it("should promise an array of results", () => {
    //   let params = <s3g.IS3ObjectGenerateRequest> {
    //       Bucket: testBucketName,
    //       Quantity: 2,
    //       Client: s3,
    //   }
    //   return expect(s3g.generate(params)).eventually.to.be.instanceof(Array);
    // });

  it("should async add 100 objects", () => {
      let params = <IS3ObjectGenerateRequest> {
          Bucket: testBucketName,
          Client: s3,
          Quantity: 100,
      };
      return generate(params)
        .then((data: any) => {
          return expect(params.Client.listObjectsV2({ Bucket: testBucketName }).promise(),
          ).eventually.to.have.property("KeyCount").to.equal(100);
    });
  });

  it("should sync add 100 objects", () => {
      let params = <IS3ObjectGenerateRequest> {
          Bucket: testBucketName,
          Client: s3,
          Quantity: 100,
      };
      generateSync(params);
      return expect(params.Client.listObjectsV2({ Bucket: testBucketName }).promise(),
      ).eventually.to.have.property("KeyCount").to.equal(100);
  });
});
