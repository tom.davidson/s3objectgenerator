import * as S3 from "aws-sdk/clients/s3";
import * as chai from "chai";
import {
  rand,
} from "./utils";

const expect = chai.expect;

describe("rand", () => {
  it("should return string", () => {
    expect(rand(10)).to.be.string;
  });
});
