import {
  generate,
  generateSync,
  IS3ObjectGenerateRequest,
} from "./s3-object-generator";

export {
  generate,
  generateSync,
  IS3ObjectGenerateRequest,
};
