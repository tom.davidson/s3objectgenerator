import * as S3 from "aws-sdk/clients/s3";
import { rand } from "./utils";

export interface IS3ObjectGenerateRequest {
  Bucket: S3.Types.BucketName;
  Quantity: number;
  Client: S3;
}

export function generate(request: IS3ObjectGenerateRequest): Promise<any> {

  let params: any = {};
  params.Bucket = request.Bucket;
  const puts: any = [];

  do {
    params.Body = rand(request.Quantity / 5) + "name.file";
    params.Key = params.Body.substring(0, 1000);
    puts.push(request.Client.putObject(params).promise());
    request.Quantity--;
  } while (request.Quantity > 0);

  return Promise.all(puts);
}

export async function generateSync(request: IS3ObjectGenerateRequest): Promise<any> {
  try {
    return await generate(request);
  } catch (err) {
    return Promise.reject(err);
  }
}
