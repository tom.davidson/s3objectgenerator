# S3ObjectGenerator

A dev tool that generates objects in an AWS S3 Bucket. Creates sudo-random
prefixes object names with prefixes. Currently only puts empty although named 
objects.

## Status

WIP. Complete testing needs a few kinks worked out before 1.0.

## Usage

Created to help in integration testing of [s3nuke](https://gitlab.com/tom.davidson/s3nuke),
this usage example is from a spec file.

```js
import * as s3g from "s3objectgenerator";

...

const generateParams: s3g.IS3ObjectGenerateRequest = {
  Bucket: testBucketName,
  Client: s3,
  Quantity: 1365,
};

it("should empty the bucket", () => {
  s3g.generateSync(generateParams);

...

```
